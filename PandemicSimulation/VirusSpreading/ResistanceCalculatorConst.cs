﻿using PandemicSimulation.Interfaces;
using System;

namespace PandemicSimulation.VirusSpreading
{
    public class ResistanceCalculatorConst : IResistanceCalculator
    {
        public double Calculate(int age)
        {
            var x = age - 50;
            var basicResistance = (-x * x + 50 * 50) / (50 * 50); // [0, 1] when age [0, 100]

            return basicResistance;
        }
    }
}
