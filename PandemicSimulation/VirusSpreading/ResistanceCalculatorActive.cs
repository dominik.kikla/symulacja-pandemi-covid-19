﻿using PandemicSimulation.Config;
using PandemicSimulation.Interfaces;

namespace PandemicSimulation.VirusSpreading
{
    public class ResistanceCalculatorActive : IResistanceCalculator
    {
        public double Calculate(int age)
        {
            double ageSusceptibility;

            switch(age)
            {
                case <= 35:
                    ageSusceptibility = -(age-70d)*age/1000d;
                    break;
                default:
                    ageSusceptibility = (age-195d)*(age-35d)/13000 + 1.225;
                    break;
            }

            return 1 - (ageSusceptibility * AppConfig.BasicSusceptibility);
        }
    }
}
