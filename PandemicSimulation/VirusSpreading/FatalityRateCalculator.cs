﻿namespace PandemicSimulation.VirusSpreading
{
    public class FatalityRateCalculator
    {
        public double Calculate(int age)
        {
            var fatalityRate = age switch
            {
                < 20 => 0.000009,
                < 30 => 0.00012,
                < 40 => 0.00035,
                < 50 => 0.00109,
                < 60 => 0.0034,
                < 70 => 0.0107,
                < 75 => 0.0157,
                < 85 => 0.0331,
                >= 85 => age / 85 * 0.127,
            };

            return fatalityRate;
        }
    }
}
