﻿using System;

namespace PandemicSimulation.Simulation;

public class Virus
{
    public float Contagiousness;
    public int InfectionRange;

    public void TryInfect(Human human)
    {
        if (human.Status == Status.Infected) return;

        var random = new Random();
        if (random.NextDouble() * Contagiousness > human.Resistance) human.Status = Status.Infected;
    }
}