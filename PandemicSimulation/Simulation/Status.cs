﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace PandemicSimulation.Simulation;

public enum Status
{
    Undefined,
    Healthy,
    Infected,
    Immune,
    Dead
}

public static class StatusExtentions
{
    public static Dictionary<Status, Texture2D> Textures;

    public static Texture2D GetTexture(this Status status)
    {
        return Textures[status];
    }
}