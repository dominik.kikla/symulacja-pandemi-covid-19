﻿namespace PandemicSimulation.Interfaces
{
    public interface IAgeCalculator
    {
        int Calculate();
    }
}
