﻿using System.Drawing;
using PandemicSimulation.Simulation;

namespace PandemicSimulation.Moving;

public class MoveSystemE : MoveSystemBase, IMoveSystem
{
    public MoveSystemE(int spawnX, int spawnY, Human owner) : base(spawnX, spawnY, owner, 10)
    {
        Destinations = new Point[10];

        Destinations[0] = new Point(spawnX, spawnY);

        Destinations[1] = getSafePoint(spawnX - 10, spawnY);
        Destinations[2] = getSafePoint(spawnX, spawnY - 20);
        Destinations[3] = getSafePoint(spawnX + 6, spawnY - 12);
        Destinations[4] = getSafePoint(spawnX - 3, spawnY - 1);
        Destinations[5] = getSafePoint(spawnX + 8, spawnY - 7);
        Destinations[6] = getSafePoint(spawnX + 14, spawnY);
        Destinations[7] = getSafePoint(spawnX + 7, spawnY + 9);
        Destinations[8] = getSafePoint(spawnX + 10, spawnY - 10);
        Destinations[9] = getSafePoint(spawnX + 5, spawnY + 2);
    }
}