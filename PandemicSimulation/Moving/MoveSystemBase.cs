﻿using System;
using System.Drawing;
using PandemicSimulation.Simulation;

namespace PandemicSimulation.Moving;

public class MoveSystemBase
{
    protected int ActualDestinationIndex;
    protected int ActualRound;
    protected Point[] Destinations;
    protected Human Owner;
    protected Point Spawn;

    protected int TotalRoundNumber;

    protected MoveSystemBase(int spawnX, int spawnY, Human owner, int totalRoundNumber)
    {
        Spawn = new Point(spawnX, spawnY);
        Owner = owner;
        ActualRound = 0;
        ActualDestinationIndex = 0;
        TotalRoundNumber = Math.Max(totalRoundNumber, 1);
    }

    protected (int, int, bool) GotoPoint(Point dest)
    {
        int moveX = 0, moveY = 0;
        if (Owner.PosX < dest.X)
            moveX = 1;
        else if (Owner.PosX == dest.X)
            moveX = 0;
        else if (Owner.PosX > dest.X) moveX = -1;

        if (Owner.PosY < dest.Y)
            moveY = 1;
        else if (Owner.PosY == dest.Y)
            moveY = 0;
        else if (Owner.PosY > dest.Y) moveY = -1;

        var nextDest = moveX == 0 && moveY == 0;

        return (moveX, moveY, nextDest);
    }

    protected void AfterMove(bool isDestReached = false)
    {
        ActualRound = (ActualRound + 1) % TotalRoundNumber;
        if (isDestReached) ActualDestinationIndex = (ActualDestinationIndex + 1) % Destinations.Length;
    }

    public (int, int) Move()
    {
        int moveX, moveY;
        bool isDestReached;
        (moveX, moveY, isDestReached) = GotoPoint(Destinations[ActualDestinationIndex]);
        AfterMove(isDestReached);
        return (moveX, moveY);
    }

    public (int, int) WaitMove()
    {
        return (0, 0);
    }

    public Point getSafePoint(int x, int y)
    {
        return Owner.Owner.getSafePoint(x, y);
    }
}