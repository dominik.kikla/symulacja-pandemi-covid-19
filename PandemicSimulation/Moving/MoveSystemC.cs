﻿using System.Drawing;
using PandemicSimulation.Simulation;

namespace PandemicSimulation.Moving;

public class MoveSystemC : MoveSystemBase, IMoveSystem
{
    public MoveSystemC(int spawnX, int spawnY, Human owner) : base(spawnX, spawnY, owner, 15)
    {
        Destinations = new Point[11];

        Destinations[0] = new Point(spawnX, spawnY);
        Destinations[1] = new Point(spawnX, spawnY);
        Destinations[2] = new Point(spawnX, spawnY);
        Destinations[3] = getSafePoint(spawnX + 8, spawnY + 5);
        Destinations[4] = getSafePoint(spawnX + 8, spawnY + 5);
        Destinations[5] = getSafePoint(spawnX + 8, spawnY + 5);
        Destinations[6] = getSafePoint(spawnX + 8, spawnY + 5);
        Destinations[7] = getSafePoint(spawnX + 3, spawnY + 2);
        Destinations[8] = getSafePoint(spawnX + 3, spawnY + 2);
        Destinations[9] = getSafePoint(spawnX + 3, spawnY + 2);
        Destinations[10] = getSafePoint(spawnX + 3, spawnY + 2);
    }
}