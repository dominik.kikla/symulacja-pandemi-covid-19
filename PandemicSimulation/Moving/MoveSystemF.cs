﻿using System.Drawing;
using PandemicSimulation.Simulation;

namespace PandemicSimulation.Moving;

public class MoveSystemF : MoveSystemBase, IMoveSystem
{
    public MoveSystemF(int spawnX, int spawnY, Human owner) : base(spawnX, spawnY, owner, 10000)
    {
        Destinations = new Point[3];

        Destinations[0] = new Point(spawnX, spawnY);
        Destinations[1] = getSafePoint(spawnX + 50, spawnY + 30);
        Destinations[2] = getSafePoint(spawnX + 25, spawnY + 5);
    }

    public (int, int) Move()
    {
        if ((ActualDestinationIndex == 0 && ActualRound < 8) || (ActualDestinationIndex == 1 && ActualRound < 12) ||
            (ActualDestinationIndex == 2 && ActualRound < 2))
            return WaitMove();

        int moveX, moveY;
        bool isDestReached;
        (moveX, moveY, isDestReached) = GotoPoint(Destinations[ActualDestinationIndex]);
        AfterMove(isDestReached);

        if (isDestReached) ActualRound = 0;

        return (moveX, moveY);
    }
}