﻿using System.Drawing;
using PandemicSimulation.Simulation;

namespace PandemicSimulation.Moving;

public class MoveSystemD : MoveSystemBase, IMoveSystem
{
    public MoveSystemD(int spawnX, int spawnY, Human owner) : base(spawnX, spawnY, owner, 15)
    {
        Destinations = new Point[6];

        Destinations[0] = new Point(spawnX, spawnY);
        Destinations[1] = getSafePoint(spawnX + 30, spawnY + 30);
        Destinations[2] = getSafePoint(spawnX + 30, spawnY + 15);
        Destinations[3] = getSafePoint(spawnX - 15, spawnY);
        Destinations[4] = getSafePoint(spawnX + 15, spawnY - 15);
        Destinations[5] = getSafePoint(spawnX, spawnY - 30);
    }
}